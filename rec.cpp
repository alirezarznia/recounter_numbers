// Recursive CPP program to find n-th Rencontres
// Number
#include <bits/stdc++.h>
using namespace std;
long long dp[101][101];
// Returns value of Binomial Coefficient C(n, k)
int binomialCoeff(int n, int k)
{
    // Base Cases
    if (k == 0 || k == n)
        return 1;

    // Recurrence relation
    return binomialCoeff(n - 1, k - 1) +
           binomialCoeff(n - 1, k);
}

// Return Recontres number D(n, m)
int RencontresNumber(int n, int m)
{
    // base condition
    if(dp[n][m] != -1) return dp[n][m];
    if (n == 0 && m == 0)
        return 1;

    // base condition
    if (n == 1 && m == 0)
        return 0;

    // base condition
    if (m == 0)
        return dp[n][m]=(n - 1) * (RencontresNumber(n - 1, 0) +
                          RencontresNumber(n - 2, 0));

    return dp[n][m]= binomialCoeff(n, m) * RencontresNumber(n - m, 0);
}

// Driver Program
int main()
{
    memset(dp , -1 , sizeof(dp));
    int n = 7, m = 2;
    cout << RencontresNumber(n, m) << endl;
    return 0;
}
