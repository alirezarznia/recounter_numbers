//D(0, 0) = 1
//D(0, 1) = 0
//D(n+2, 0) = (n+1) * (D(n+1, 0) + D(n, 0))
//D(n, k) = nCk * D(n-k, 0))


// DP based CPP program to find n-th Rencontres
// Number
#include <bits/stdc++.h>
using namespace std;
#define MAX 100

// Fills table C[n+1][k+1] such that C[i][j]
// represents table of binomial coefficient
// iCj
int binomialCoeff(int C[][MAX], int n, int k)
{
    // Calculate value of Binomial Coefficient
    // in bottom up manner
    for (int i = 0; i <= n; i++) {
        for (int j = 0; j <= min(i, k); j++) {

            // Base Cases
            if (j == 0 || j == i)
                C[i][j] = 1;

            // Calculate value using previously
            // stored values
            else
                C[i][j] = C[i - 1][j - 1] +
                          C[i - 1][j];
        }
    }
}

// Return Recontres number D(n, m)
int RencontresNumber(int C[][MAX], int n, int m)
{
    int dp[n+1][m+1] = { 0 };
int j= 0;
    for (int i = 0; i <= n-m; i++) {
                // base case
                if (i == 0 && j == 0)
                    dp[i][j] = 1;

                // base case
                else if (i == 1 && j == 0)
                    dp[i][j] = 0;

                else if (j == 0)
                    dp[i][j] = (i - 1) * (dp[i - 1][0] +
                                          dp[i - 2][0]);
    }


    return C[n][m] * dp[n-m][0];
}

// Driver Program
int main()
{
    int n = 7, m = 2;

    int C[MAX][MAX];
    binomialCoeff(C, n, m);

    cout << RencontresNumber(C, n, m) << endl;
    return 0;
}
